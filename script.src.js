function redondear(n, decimales = 2) {
  const potencia = 10**decimales
  return Math.round(n * potencia) / potencia
}

function acumuladas(datos, rasero) {
  return [].filter.call(datos, i => i >= rasero).length
}

function obtenerDatos(miNota) {
  const nodos = document.querySelectorAll("table.upv_listacolumnas > tbody > tr > td:nth-of-type(2)")
  const notas = [].map.call(nodos, n => Number(n.innerText.replace(",", "."))).filter(i => i === i).sort()
  const len = notas.length
  
  let acumulada = 0
  
  const result = {
    "No. de notas": len,
    "Max": notas[len - 1],
    "Min": notas[0],
    "Media": Math.round((notas.reduce((acc, n) => acc + n, 0) / len)*100)/100,
    "Primer cuartil": notas[Math.ceil(len / 4)],
    "Mediana": notas[Math.ceil(len / 2)],
    "Tercer cuartil": notas[Math.ceil(3 * len / 4)],
    "Notas de 1 o mejor": `${acumulada = acumuladas(notas, 1)} (${redondear(100 * acumulada / len)}%)`,
    "Notas de 2 o mejor": `${acumulada = acumuladas(notas, 2)} (${redondear(100 * acumulada / len)}%)`,
    "Notas de 3 o mejor": `${acumulada = acumuladas(notas, 3)} (${redondear(100 * acumulada / len)}%)`,
    "Notas de 4 o mejor": `${acumulada = acumuladas(notas, 4)} (${redondear(100 * acumulada / len)}%)`,
    "Notas de 5 o mejor": `${acumulada = acumuladas(notas, 5)} (${redondear(100 * acumulada / len)}%)`,
    "Notas de 6 o mejor": `${acumulada = acumuladas(notas, 6)} (${redondear(100 * acumulada / len)}%)`,
    "Notas de 7 o mejor": `${acumulada = acumuladas(notas, 7)} (${redondear(100 * acumulada / len)}%)`,
    "Notas de 8 o mejor": `${acumulada = acumuladas(notas, 8)} (${redondear(100 * acumulada / len)}%)`,
    "Notas de 9 o mejor": `${acumulada = acumuladas(notas, 9)} (${redondear(100 * acumulada / len)}%)`,
    "Notas de 10": `${acumulada = acumuladas(notas, 10)} (${redondear(100 * acumulada / len)}%)`,
  }
  result["Aprobados"] = result["Notas de 5 o mejor"]
  
  if (miNota) {
    try {
      if (miNota !== miNota) throw "No he entendido tu nota. Usa puntos o comas para separar los decimales."
      const i = notas.lastIndexOf(miNota)
      if (i < 0) throw "No he encontrado tu nota en la lista. Tecleala tal y como aparece."
      result["Tu nota"] = `${len - i - 1} personas han sacado mejor nota que la tuya`
    } catch (msg) {
      result["Error"] = msg
    }
  }
  
  return result
  
}

const comparar = + prompt("Escribe tu nota si quieres compararla").replace(",", ".")

const datos = obtenerDatos(comparar)
let texto = ""
for (const prop in datos) {
  texto += `${prop}: ${datos[prop]}\n`
}

alert(texto)